import React, {useEffect, useState} from 'react';

function ConferenceForm() {
    const[locations,setLocations] = useState([]);

    const fetchData = async () => {
        const locationUrl = 'http://localhost:8000/api/locations/';
        const response = await fetch(locationUrl);

        if (!response.ok) {
            throw new Error("Response not okay!");
        } else {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect( () => {
        fetchData();
    }, []);

    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const [starts, setStartDate] = useState('');
    const handleStartDateChange = (event) => {
        const value = event.target.value;
        setStartDate(value);
    }


    const [ends, setEndDate] = useState('');
    const handleEndDateChange = (event) => {
        const value = event.target.value;
        setEndDate(value);
    }


    const [description, setDescription] = useState('');
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const [max_presentations, setMaxPresentations] = useState('');
    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }


    const [max_attendees, setMaxAttendees] = useState('');
    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }

    const [location, setLocation] = useState('');
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
            name: name,
            starts: starts,
            ends: ends,
            description: description,
            max_presentations: max_presentations,
            max_attendees: max_attendees,
            location: location,
        }

        const conferenceUrl = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": 'application/json',
            }
        }
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
            setName("");
            setStartDate("");
            setEndDate("");
            setDescription("");
            setMaxPresentations("");
            setMaxAttendees("");
            setLocation("");
        }
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new conference</h1>
                <form onSubmit={handleSubmit} id="create-conference-form">
                    <div className="form-floating mb-3">
                        <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={starts} onChange={handleStartDateChange} placeholder="starts" required type="date" name="starts" id="starts" className="form-control"/>
                        <label htmlFor="starts">Starts</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={ends} onChange={handleEndDateChange} placeholder="ends" required type="date" name="ends" id="ends" className="form-control"/>
                        <label htmlFor="ends">Ends</label>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="description" className="form-label">Description</label>
                        <textarea value={description} onChange={handleDescriptionChange} className="form-control" required name="description" id="description" rows="3"></textarea>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={max_presentations} onChange={handleMaxPresentationsChange} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                        <label htmlFor="name">Maximum Presentations</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={max_attendees} onChange={handleMaxAttendeesChange} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                        <label htmlFor="name">Maximum Attendees</label>
                    </div>
                    <div className="form-floating mb-3">
                        <select value={location} onChange={handleLocationChange} required name="location" id="location" className="form-select">
                            <option value="">Choose a location</option>
                            {locations.map(location => {
                                return (
                                    <option key={location.id} value={location.id}>{location.name}</option>
                                );
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    )
}

export default ConferenceForm;
