import React, {useEffect, useState} from 'react';

function PresentationForm() {
    const[conferences, setConferences] = useState([]);

    const fetchData = async () => {
        const conferencesUrl = 'http://localhost:8000/api/conferences/';
        const response = await fetch(conferencesUrl);

        if (!response.ok) {
            throw new Error("Response not okay!");
        } else {
            const data = await response.json();
            setConferences(data.conferences);
        }
    }

    useEffect( () => {
        fetchData();
    },[]);

    const[presenterName, setPresenterName] = useState('');
    const handlePresenterNameChange = (event) => {
        const value = event.target.value;
        setPresenterName(value);
    }

    const[presenterEmail, setPresenterEmail] = useState('');
    const handlePresenterEmailChange = (event) => {
        const value = event.target.value;
        setPresenterEmail(value);
    }
    const[companyName, setCompanyName] = useState('');
    const handleCompanyNameChange = (event) => {
        const value = event.target.value;
        setCompanyName(value);
    }

    const[title, setTitle] = useState('');
    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
    }

    const[synopsis, setSynopsis] = useState('');
    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }

    const[conference, setConference] = useState('');
    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
            presenter_name: presenterName,
            presenter_email: presenterEmail,
            company_name: companyName,
            title: title,
            synopsis: synopsis,
        }
        const presentationUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers : {
                "Content-Type" : 'application/json',
            }
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);
            setPresenterName("");
            setPresenterEmail("");
            setCompanyName("");
            setTitle("");
            setSynopsis("");
            setConference("");
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new presentation</h1>
                    <form onSubmit={handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input value={presenterName} onChange={handlePresenterNameChange} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                            <label htmlFor="presenter_name">Presenter name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={presenterEmail} onChange={handlePresenterEmailChange} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                            <label htmlFor="presenter_email">Presenter email</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={companyName} onChange={handleCompanyNameChange} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
                            <label htmlFor="company_name">Company name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={title} onChange={handleTitleChange} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                            <label htmlFor="presenter_email">Title</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="presenter_email" className="form-label">Synopsis</label>
                            <textarea value={synopsis} onChange={handleSynopsisChange} className="form-control" required name="synopsis" id="synopsis" rows="3"></textarea>
                        </div>
                        <div className="form-floating mb-3">
                            <select value={conference} onChange={handleConferenceChange} required name="conference" id="conference" className="form-select">
                                <option value="">Choose a conference</option>
                                {conferences.map(conference => {
                                    return (
                                        <option key={conference.id} value={conference.id} >{conference.name}</option>
                                    );
                                })}
                            </select>
                        </div>

                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
    );
}

export default PresentationForm;
